﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace StockerBobby_CE01
{
	public partial class MainForm : Form
	{
		// List of DVD objects
		List<DVD> dvdList = new List<DVD>();

		XmlDocument stockDocument = new XmlDocument();

		// Sql Connection
		MySqlConnection _con = new MySqlConnection();

		// Datatable to hold data
		DataTable data = new DataTable();


		public MainForm()
		{
			InitializeComponent();

			listBox1.DisplayMember = "DVD_Title";
			listBox2.DisplayMember = "DVD_Title";
		}

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				// Show the user a dialog to select the proper file
				using (OpenFileDialog openFileDialog = new OpenFileDialog())
				{
					openFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
					openFileDialog.DefaultExt = "xml";

					// Show the dialog
					if (openFileDialog.ShowDialog() == DialogResult.OK)
					{
						XmlSerializer serializer = new XmlSerializer(typeof(List<DVD>));
						// Stream in the file and deserialize the data
						using (FileStream stream = File.OpenRead(openFileDialog.FileName))
						{
							// Populate the list
							dvdList = (List<DVD>)serializer.Deserialize(stream);
						}
					}
				}
				// set up the display
				PopulateListView();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				// Gather the data
				dvdList.Clear();
				for (int i = 0; i < listBox1.Items.Count; i++) {
					dvdList.Add(listBox1.Items[i] as DVD);
				}
				for (int i = 0; i < listBox2.Items.Count; i++)
				{
					dvdList.Add(listBox2.Items[i] as DVD);
				}
				//Get the save dialog prepped
				using (SaveFileDialog saveFileDialog = new SaveFileDialog())
				{
					saveFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
					saveFileDialog.DefaultExt = "xml";
					saveFileDialog.InitialDirectory = string.Empty;

					// Show the savedialog 
					if (saveFileDialog.ShowDialog() == DialogResult.OK) {
						if (saveFileDialog.FileName != "") {

							// Save the data
							XmlSerializer serialiser = new XmlSerializer(typeof(List<DVD>));
							TextWriter FileStream = new StreamWriter(saveFileDialog.FileName, false);
							serialiser.Serialize(FileStream, dvdList);
							FileStream.Close();
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void printListToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void moveRightButton_Click(object sender, EventArgs e)
		{
			// Make sure that something is selected first
			if (listBox1.SelectedItems.Count > 0) {
				// move selected item
				MoveItemToListBox(listBox1, listBox2);
			}
		}

		/// <summary>
		/// Move select items from one list box to another
		/// </summary>
		/// <param name="source"></param>
		/// <param name="target"></param>
		private void MoveItemToListBox(ListBox source, ListBox target)
		{
			// Get the selected object
			DVD selectedItem = source.SelectedItem as DVD;
			
			// Set which listbox this will be moved to
			if(target == listBox2) {
				selectedItem.DisplayList = 2;
			} else
			{
				selectedItem.DisplayList = 1;
			}

			// Move (actually copies) the object to the destination
			target.Items.Add(selectedItem);

			// remove the moved item from the source
			source.Items.Remove(source.SelectedItem);
		}

		private void moveLeftButton_Click(object sender, EventArgs e)
		{
			// Make sure that something is selected first
			if (listBox2.SelectedItems.Count > 0) {
				// move selected item
				MoveItemToListBox(listBox2, listBox1);
			}
		}

		private void getDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			// Build the connection string
			BuildConString();

			// Connect to the database
			Connect();

			// Get Data
			RetrieveData();

			// Display the data
			PopulateListView();
		}

		void BuildConString()
		{
			string ip = "";
			try
			{
				// get the file from @"C:\vfw\connect.txt"
				using (StreamReader sr = new StreamReader("c:\\connect.txt"))
				{
					ip = sr.ReadLine();

					// Check to see if the ip address has a value
					// If not, the use localhost
					if (string.IsNullOrWhiteSpace(ip))
					{
						ip = "localhost";
					}
				}

				string conString = $"Server={ip};";
				conString += "uid=dbsAdmin;";
				conString += "pwd=password;";
				conString += "database=exampleDatabase;";
				conString += "port=8889";

				_con.ConnectionString = conString;
			}
			catch (Exception)
			{

				MessageBox.Show("Error trying to build SQL connection string!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}

		}

		void Connect()
		{
			try
			{
				// Attepmt to connection to the MySQL server
				_con.Open();
				//MessageBox.Show("Connected!");
				
			}
			catch (MySqlException ex)
			{
				// need a variable for the message
				string msg = "";

				// Get the error
				switch (ex.Number)
				{
					case 0:
						msg = ex.ToString();
						break;
					case 1042:
						msg = "Cannot resolve host address.\n";
						break;
					case 1045:
						msg = "Invalid username/password";
						break;
					default:
						msg = ex.ToString() + "\n" + _con.ConnectionString;
						break;

				}
				
				MessageBox.Show(msg);
			}
		}

		void RetrieveData()
		{
			// Sql Statement
			string sqlQuery = "SELECT dvdId, DVD_Title, Year, Price, publicRating  FROM exampleDatabase.dvd Limit 20;";

			try
			{
				// Data adapter for getting data
				MySqlDataAdapter msda = new MySqlDataAdapter(sqlQuery, _con);

				// set command type
				msda.SelectCommand.CommandType = CommandType.Text;

				// Get the data
				msda.Fill(data);

				// Put first record into display
				//DisplayRecord(0);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

		}

		void PopulateListView()
		{
			// Do we data to work with?
			if (dvdList.Count < 1) return;

			// Loop through the data and build out the objects for the listView
			for (int i = 0; i < dvdList.Count; i++)
			{
				if (dvdList[i].DisplayList.Equals(1)) {
					// Add DVD to proper listbox
					listBox1.Items.Add(dvdList[i]);
				} else {
					listBox2.Items.Add(dvdList[i]);
				}
				
				
				// close the sql connection
				//_con.Close();
			}
		}

		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			listBox1.ForeColor = Color.Red;
			listBox2.ForeColor = Color.Red;
		}


		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{
			listBox1.ForeColor = Color.Blue;
			listBox2.ForeColor = Color.Blue;
		}

		private void deleteButton_Click(object sender, EventArgs e)
		{
			// delete selected item from listboxes
			if(listBox1.SelectedItems.Count > 0)
			{
				listBox1.Items.RemoveAt(listBox1.SelectedIndex);
			}

			// check the second listbox
			if (listBox2.SelectedItems.Count > 0)
			{
				listBox2.Items.RemoveAt(listBox2.SelectedIndex);
			}
		}

		private void createDVDButton_Click(object sender, EventArgs e)
		{
			// Create an instance of the input form
			DVDInputForm dif = new DVDInputForm();
			// Add listener
			dif.DVDAdded += NewDVDHandler;

			dif.ShowDialog();
		}

		private void NewDVDHandler(object sender, EventArgs e)
		{
			// Get the new dvd from the input form
			DVD newDVD;
			DVDInputForm dif = sender as DVDInputForm;
			newDVD = dif.NewDVD;

			// add it to the master list just in case
			dvdList.Add(newDVD);

			if (newDVD.DisplayList.Equals(1)) {
				listBox1.Items.Add(newDVD);
			} else
			{
				listBox2.Items.Add(newDVD);
			}
		}
	}
}
