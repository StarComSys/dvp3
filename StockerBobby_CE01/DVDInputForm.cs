﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockerBobby_CE01
{
	public partial class DVDInputForm : Form
	{
		public EventHandler DVDAdded;

		public DVD NewDVD { get; set; }

		public DVDInputForm()
		{
			InitializeComponent();
		}

		private void saveButton_Click(object sender, EventArgs e)
		{
			// Create the new DVD
			NewDVD = new DVD()
			{
				DvdId = RandomNumber(),
				Name = DvdNameTextBox.Text,
				Year = Convert.ToInt32(ReleaseYearTextBox.Text),
				Price = DvdPrice.Value,
				Rating = StarRating.Value,
			};
			// Set the DVD for the proper List
			if (hasWatched.Checked) {
				NewDVD.Watched = true;
				NewDVD.DisplayList = 2;
			} else
			{
				NewDVD.Watched = false;
				NewDVD.DisplayList = 1;
			}

			// trigger the event
			this.DVDAdded?.Invoke(this, new EventArgs());

			this.Close();
		}
		// Generate a random number between two numbers.
		public int RandomNumber()
		{
			Random random = new Random();
			return random.Next(1000, 5000);
		}
	}
}
