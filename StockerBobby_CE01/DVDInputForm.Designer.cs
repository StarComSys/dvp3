﻿namespace StockerBobby_CE01
{
	partial class DVDInputForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox = new System.Windows.Forms.GroupBox();
			this.ReleaseYearTextBox = new System.Windows.Forms.MaskedTextBox();
			this.saveButton = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.StarRating = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.DvdPrice = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.DvdNameTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.hasWatched = new System.Windows.Forms.CheckBox();
			this.groupBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.StarRating)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DvdPrice)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox
			// 
			this.groupBox.Controls.Add(this.hasWatched);
			this.groupBox.Controls.Add(this.ReleaseYearTextBox);
			this.groupBox.Controls.Add(this.saveButton);
			this.groupBox.Controls.Add(this.label6);
			this.groupBox.Controls.Add(this.StarRating);
			this.groupBox.Controls.Add(this.label5);
			this.groupBox.Controls.Add(this.DvdPrice);
			this.groupBox.Controls.Add(this.label4);
			this.groupBox.Controls.Add(this.label3);
			this.groupBox.Controls.Add(this.DvdNameTextBox);
			this.groupBox.Controls.Add(this.label2);
			this.groupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox.Location = new System.Drawing.Point(12, 12);
			this.groupBox.Name = "groupBox";
			this.groupBox.Size = new System.Drawing.Size(380, 398);
			this.groupBox.TabIndex = 4;
			this.groupBox.TabStop = false;
			this.groupBox.Text = "DVD Viewer";
			// 
			// ReleaseYearTextBox
			// 
			this.ReleaseYearTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ReleaseYearTextBox.Location = new System.Drawing.Point(122, 59);
			this.ReleaseYearTextBox.Mask = "0000";
			this.ReleaseYearTextBox.Name = "ReleaseYearTextBox";
			this.ReleaseYearTextBox.Size = new System.Drawing.Size(120, 26);
			this.ReleaseYearTextBox.TabIndex = 1;
			this.ReleaseYearTextBox.ValidatingType = typeof(int);
			// 
			// saveButton
			// 
			this.saveButton.Location = new System.Drawing.Point(100, 285);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(177, 32);
			this.saveButton.TabIndex = 21;
			this.saveButton.Text = "Save Changes";
			this.saveButton.UseVisualStyleBackColor = true;
			this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(202, 125);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(47, 20);
			this.label6.TabIndex = 20;
			this.label6.Text = "Stars";
			// 
			// StarRating
			// 
			this.StarRating.DecimalPlaces = 1;
			this.StarRating.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StarRating.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.StarRating.Location = new System.Drawing.Point(122, 123);
			this.StarRating.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.StarRating.Name = "StarRating";
			this.StarRating.Size = new System.Drawing.Size(74, 26);
			this.StarRating.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(56, 125);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(60, 20);
			this.label5.TabIndex = 18;
			this.label5.Text = "Rating:";
			// 
			// DvdPrice
			// 
			this.DvdPrice.DecimalPlaces = 2;
			this.DvdPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DvdPrice.Location = new System.Drawing.Point(122, 91);
			this.DvdPrice.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.DvdPrice.Name = "DvdPrice";
			this.DvdPrice.Size = new System.Drawing.Size(120, 26);
			this.DvdPrice.TabIndex = 2;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(68, 93);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 20);
			this.label4.TabIndex = 16;
			this.label4.Text = "Price:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(6, 62);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(110, 20);
			this.label3.TabIndex = 8;
			this.label3.Text = "Release Year:";
			// 
			// DvdNameTextBox
			// 
			this.DvdNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DvdNameTextBox.Location = new System.Drawing.Point(122, 27);
			this.DvdNameTextBox.Name = "DvdNameTextBox";
			this.DvdNameTextBox.Size = new System.Drawing.Size(246, 26);
			this.DvdNameTextBox.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(22, 30);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 20);
			this.label2.TabIndex = 6;
			this.label2.Text = "DVD Name:";
			// 
			// hasWatched
			// 
			this.hasWatched.AutoSize = true;
			this.hasWatched.Location = new System.Drawing.Point(122, 166);
			this.hasWatched.Name = "hasWatched";
			this.hasWatched.Size = new System.Drawing.Size(116, 20);
			this.hasWatched.TabIndex = 22;
			this.hasWatched.Text = "Has watched";
			this.hasWatched.UseVisualStyleBackColor = true;
			// 
			// DVDInputForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(404, 450);
			this.Controls.Add(this.groupBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "DVDInputForm";
			this.Text = "DVDInput";
			this.groupBox.ResumeLayout(false);
			this.groupBox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.StarRating)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DvdPrice)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox;
		private System.Windows.Forms.MaskedTextBox ReleaseYearTextBox;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown StarRating;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown DvdPrice;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox DvdNameTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox hasWatched;
	}
}