﻿namespace StockerBobby_CE01
{
	public class DVD
	{
		public int DvdId { get; set; }
		public string Name { get; set; }
		public int Year { get; set; }
		public decimal Price { get; set; }
		public decimal Rating { get; set; }
		public bool Watched { get; set; }
		public int DisplayList { get; set; }


		public DVD(int id, string name, int year, decimal price, decimal rating, 
					bool watched, int displayList, bool didWatch, int listNumber)
		{
			this.DvdId = id;
			this.Name = name;
			this.Year = year;
			this.Price = price;
			this.Rating = rating;
			this.Watched = watched;
			this.DisplayList = displayList;
			this.Watched = didWatch;
			this.DisplayList = listNumber;
		}

		public DVD()
		{
		}

		public override string ToString()
		{
			return string.Format("{0,-65}", Name) + $"\tYear: {Year}" + $"\tPrice: {Price}\tRating: {Rating}";
		}

	}
}
